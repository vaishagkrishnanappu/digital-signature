from random import randint
from hashlib import sha1
from Crypto.Util.number import getPrime
import PyPDF2
import sys
import datetime


def getHashedMessage(inp):
    op = sha1(inp.encode("UTF-8")).hexdigest()
    return op


def getModInverse(a,b):
    a %= b
    for i in range(1,b):
        if (a * i) % b == 1:
            return i
    return 1


def getGlobalComponents():
    p = getPrime(10)
    q = getPrime(5)
    while (p - 1) % q != 0:
        p = getPrime(10)
        q = getPrime(5)

    h = randint(1, p-1)
    g = (h ** ((p - 1)//q)) % p
    return p, q, g


def userPrivateKey(q):
    x = randint(0, q)
    return x


def userPublicKey(p,g,x):
    y = (g ** x) % p
    return y


def getSignatureComponents(h,p,q,g,x):
    h = int(h, 16)
    r, s = 0, 0
    while r <= 1 or s == 0:
        k = randint(0, q)
        i = getModInverse(k, q)
        r = ((g ** k) % p) % q
        s = (i * (h + (x * r))) % q
    return r, s


def getText(fileName):
    file = open(fileName, "rb")
    pdf = PyPDF2.PdfFileReader(file,strict=False)
    pgno = pdf.numPages
    op = ""
    for i in range(pgno):
        op += pdf.getPage(i).extractText()
    file.close()
    return op


def addSign(fileName,signature):
    ipFile = open(fileName,"rb")
    pdf = PyPDF2.PdfFileReader(ipFile,strict=False)
    pgNo= pdf.numPages
    out = PyPDF2.PdfFileWriter()
    out.addMetadata(signature)
    for i in range(pgNo):
        out.addPage(pdf.getPage(i))
    opFileName = "C:\\xampp\\htdocs\\signature\\py\\Signed.pdf"
    opFile = open(opFileName,"wb")
    out.write(opFile)
    opFile.close()
    ipFile.close()


fileName = sys.argv[1]
author = sys.argv[2]
email = sys.argv[3]
time = datetime.datetime.now()
date = str(time.day)+"-"+str(time.month)+"-"+str(time.year)
#print(date,fileName,author,email)
if ".pdf" not in fileName:
    fileName += ".pdf"
txt = getText(fileName)
#print(txt)
hashedmsg = getHashedMessage(txt)
p, q, g = getGlobalComponents()
x = userPrivateKey(q)
y = userPublicKey(p,g,x)
r, s = getSignatureComponents(hashedmsg,p,q,g,x)
#print(hashedmsg,p,q,g,x,k,r,s,sep="\n")
signature = {
    '/p': str(p),
    '/q': str(q),
    '/g': str(g),
    '/y': str(y),
    '/r': str(r),
    '/s': str(s),
    '/author' : author,
    '/email' : email,
    '/date' : date
}
#print(signature)
addSign(fileName,signature)
print("Sucess")
