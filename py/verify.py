import PyPDF2
from hashlib import sha1
import sys


def getHashedMessage(inp):
    op = sha1(inp.encode("UTF-8")).hexdigest()
    return op


def getModInverse(a,b):
    a %= b
    for i in range(1,b):
        if (a * i) % b == 1:
            return i
    return 1


def getVerifyingComponent(p,q,g,y,r,s,h):
    h = int(h, 16)
    w =getModInverse(s, q)
    u1 = (h * w) % q
    u2 = (r * w) % q

    v = int((((g ** u1) * (y ** u2)) % p) % q)
    return v


def verifySign(v,r):
    if v == r:
        return True
    else:
        return False

def getSignatureComponents(fileName):
    file = open(fileName, "rb")
    pdf = PyPDF2.PdfFileReader(file,strict=False)
    pgNo = pdf.numPages
    signature = pdf.getDocumentInfo()
    txt = ""
    for i in range(pgNo):
        txt += pdf.getPage(i).extractText()
    return txt, signature


fileName = sys.argv[1]
if ".pdf" not in fileName:
    fileName += ".pdf"
msg, signature = getSignatureComponents(fileName)
try:
    p = int(signature['/p'])
    q = int(signature['/q'])
    g = int(signature['/g'])
    y = int(signature['/y'])
    r = int(signature['/r'])
    s = int(signature['/s'])
    author = signature['/author']
    email = signature['/email']
    date = signature['/date']
    #print(author,email,date)
    h = getHashedMessage(msg)
    v = getVerifyingComponent(p, q, g, y, r, s, h)
    if verifySign(v, r):
        print("<h3 style='color:green'>Valid Signature</h3>")
        print("<h3>Author : "+author+"</h3>")
        print("<h3>Email : "+email+"</h3>")
        print("<h3>Date : "+date+"</h3>")
        #print(r, v)
    else:
        #print(r,v)
        print("<h2 style='color:red'>Invalid Signature</h2>")

except:
    print("No signature found")
